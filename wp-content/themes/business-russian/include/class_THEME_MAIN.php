<?php
/*********************************************
Описание основного класса управления темой
by DStaroselskiy 

Version: 0.1
Date: 2016-05-03
*********************************************/
namespace DStaroselskiy\Theme\Business_Russian;

if( !class_exists('\DStaroselskiy\Theme\Business_Russian\COSTOMIZER') ) { require_once( DMS_THEME_TEMPLATE_DIR . '/include/class_COSTOMIZER.php'); }

class THEME_MAIN extends \DStaroselskiy\Theme\Business_Russian\COSTOMIZER {
	
	
	// Подключаем размер дополнительных миниатюр к админке
	function thumbnails_custom_sizes( $sizes ) {
		return array_merge( $sizes, array(
			//'category-thumb' => 'Мой размерчик',
			//'homepage-thumb' => __(''),
			'catalog-thumb-min' => __('Catalog thumbnails min','dms_theme'),
			'catalog-thumb' => __('Catalog thumbnails standart','dms_theme'),
			'catalog-thumb-max' => __('Catalog thumbnails max','dms_theme'),
		) );		
	}
	
	// Регистрируем два сайдбара для вівода виджетов с параметрами:  Один с float:left, другой с float:right 
	function register_sidebars(){
		if ( function_exists( 'register_sidebar' ) ) {
			register_sidebar( array(
				'name' => __('Left sidebar in footer','dms_theme'),
				'id' => "sidebar-footer-left",
				'description' => '',
				'class' => '',
				'before_widget' => '<li id="%1$s" class="widget %2$s">',
				'after_widget' => "</li>\n",
				'before_title' => '<h2 class="widgettitle">',
				'after_title' => "</h2>\n",
			) );
			register_sidebar( array(
				'name' => __('Right sidebar in footer','dms_theme'),
				'id' => "sidebar-footer-right",
				'description' => '',
				'class' => '',
				'before_widget' => '<li id="%1$s" class="widget %2$s">',
				'after_widget' => "</li>\n",
				'before_title' => '<h5 class="widgettitle">',
				'after_title' => "</h5>\n",
			) );
			register_sidebar( array(
				'name' => __('Right sidebar in content','dms_theme'),
				'id' => "sidebar-content-right",
				'description' => '',
				'class' => '',
				'before_widget' => '<li id="%1$s" class="widget %2$s">',
				'after_widget' => "</li>\n",
				'before_title' => '<h5 class="widgettitle">',
				'after_title' => "</h5>\n",
			) );
		}
	}
	//Подключаем файлы стилей
	public function load_style_links(){
		if( !wp_style_is( 'font-awesome', 'registered ') ) wp_enqueue_style( 'font-awesome', DMS_THEME_TEMPLATE_URL.'/css/font-awesome.min.css', array(), null ,'all');
		if( !wp_style_is( 'dms-theme-fonts', 'registered ') ) wp_enqueue_style( 'dms-theme-fonts', DMS_THEME_TEMPLATE_URL.'/css/fonts.css', array(), null ,'all');
		if( !wp_style_is( 'dms-theme-core', 'registered ') ) wp_enqueue_style( 'core', DMS_THEME_TEMPLATE_URL.'/css/core.css', array(), null ,'all');
		if( !wp_style_is( 'dms-theme-style', 'registered ') ) wp_enqueue_style( 'style', DMS_THEME_TEMPLATE_URL.'/style.css', array(), null ,'all');
	}
	//Подключаем файлы скриптов
	public function load_script_links(){
		if( !wp_script_is( 'jquery', 'registered ') ) wp_enqueue_script( 'jquery', DMS_THEME_TEMPLATE_URL.'/js/jquery-1.11.3.min.js',array(),'1.11.3',true);
		if( !wp_script_is( 'jquery.inputmask', 'registered ') ) wp_enqueue_script( 'jquery.inputmask', DMS_THEME_TEMPLATE_URL.'/js/jquery.inputmask.js',array('jquery'),null,true);
		if( !wp_script_is( 'jquery.inputmask.extensions', 'registered ') ) wp_enqueue_script( 'jquery.inputmask.extensions', DMS_THEME_TEMPLATE_URL.'/js/jquery.inputmask.extensions.js',array('jquery'),null,true);
		if( !wp_script_is( 'jquery.inputmask.regex.extensions', 'registered ') ) wp_enqueue_script( 'jquery.inputmask.regex.extensions', DMS_THEME_TEMPLATE_URL.'/js/jquery.inputmask.regex.extensions.js',array('jquery'),null,true);
		if( !wp_script_is( 'dms-theme-core', 'registered ') ) wp_enqueue_script( 'dms-theme-core', DMS_THEME_TEMPLATE_URL.'/js/core.js',array('jquery'),null,true);
	}

	public function theme_setup(){
		//Задаем размер дополнительных миниатюр
		if ( function_exists( 'add_image_size' ) ) {
			// Задаем размеры миниатюре для главной страницы
			//add_image_size('homepage-thumb', 159, 150);
			// Задаем размеры миниатюре для отображения превью в табличном режиме
			add_image_size('catalog-thumb-min', 118, 118);
			// Задаем размеры миниатюре для отображения 
			// 1) превью объявления в плиточном режиме
			// 2) превью фото в слайдере на странице объявления
			// 3) превью фото при отображения списка похожих объявлениях
			add_image_size('catalog-thumb', 200, 200);
			// Задаем размеры миниатюре для отображения:
			// 1) превью объявления в большом режиме 
			// 2) отображение главного фото в слайдере на странице объявления
			add_image_size('catalog-thumb-max', 946, 470);
			add_image_size('avatar_big', 300, 300, true);
			add_image_size('avatar_small', 59, 59, true);
		}
		
		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'dms_theme' ),
			'social'  => __( 'Social Links Menu', 'dms_theme' ),
		) );
		load_theme_textdomain( 'dms_theme', get_template_directory() . '/languages' );
		
		// Инициализация настроек темы
		if ( function_exists( 'add_theme_support' ) ) {
		
			add_theme_support( 'title-tag' );

			add_theme_support( 'custom-logo', array(
				'height'      => 225,
				'width'       => 172,
				'flex-height' => true,
			) );
			
			add_theme_support( 'custom-header', array(
				'width'         => 1920,
				'height'        => 745,
				'flex-width'    => true,
				'flex-height'    => true,
				'uploads'       => true,
			));

			add_theme_support('post-thumbnails'); 
		
			add_theme_support( 'html5', array('search-form','comment-form','comment-list','gallery','caption',) );

			add_theme_support( 'post-formats', array('aside','image','video','quote','link','gallery','status',	'audio','chat',) );
		}
		
		add_editor_style( array( 'css/editor-style.css', DMS_THEME_TEMPLATE_URL ) );

		add_theme_support( 'customize-selective-refresh-widgets' );	
	}
	
	function __construct() {
		
		parent::__construct();
		//Очищаем заготовок CMS
		remove_action( 'wp_head', 'feed_links_extra', 3 ); 
		remove_action( 'wp_head', 'feed_links', 2 );
		remove_action( 'wp_head', 'rsd_link' );
		remove_action( 'wp_head', 'index_rel_link' );
		remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); 
		remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
		remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
		remove_action( 'wp_head', 'wp_generator' );
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
		remove_action( 'wp_head', 'wp_oembed_add_host_js' );
		//Удаляем "манифест". Он подключает специальный XML-файл к сайту,  чтобы включить возможность взаимодействия блога на движке WordPress с приложением Windows Live Writer.
		remove_action('wp_head', 'wlwmanifest_link');
		
		add_action( 'after_setup_theme', array( &$this, 'theme_setup') );	
		add_action( 'wp_enqueue_scripts', array( &$this, 'load_style_links') );//Подключаем вывод основных стилей
		add_action( 'wp_enqueue_scripts', array( &$this, 'load_script_links') );//Подключаем вывод основных скриптов
		// add_filter( 'locale', array( &$this, 'set_stella_locale' ) ); //Корректируем локализацию темы от плагина STELLA
		// add_filter( 'stella_lang_name', array( &$this, 'change_lang_names' ) );//Изменяем отображение языков у плагина STELLA
		add_filter( 'image_size_names_choose', array( &$this, 'thumbnails_custom_sizes' ) );// Подключаем размер дополнительных миниатюр к админке
		add_action( 'widgets_init', array( &$this,  'register_sidebars' ) ); // Регистрируем два сайдбара для вівода виджетов с параметрами:  Один с float:left, другой с float:right 

	}
}

?>