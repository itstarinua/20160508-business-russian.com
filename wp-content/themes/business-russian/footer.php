<?php
/********************************************
*********************************************
Основной шаблон вывода подвала (footer) сайта
*********************************************
********************************************/

//Выводим два сайдбара в футаре. Один с float:left, другой с float:right ?>
	</div> <!-- End of #page-body -->
	<footer>
		<div id="footer-sidebar" class="row">
			<div class="container">
				<div class="row">
					<?php if ( function_exists('dynamic_sidebar') ) :?> 
					<ul id="sidebar-left" class="pull-left">
						<?php dynamic_sidebar('sidebar-left'); ?>
					</ul>
					<ul id="sidebar-right" class="pull-right">
						<?php dynamic_sidebar('sidebar-right'); ?>
					</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php //Выводим копирайт в футаре. ?>	
		<div id="footer-copyright" class="row text-center">
			<a href="<?php echo home_url();?>" title="<?php _e('На главную','dms_theme');?>" rel="nofollow"><em><strong>ekotorg</strong>.com.ua</em></a>  <i class="fa fa-copyright"></i> 2016
		</div>
	</footer>
<?php
	wp_footer(); // Шункция вывода скриптов ядра вордпресса
?>
</body>
</html>