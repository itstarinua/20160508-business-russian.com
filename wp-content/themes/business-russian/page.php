<?php
/********************************************
*********************************************
Основной шаблон вывода страницы сайта
Дата: 04/05/2016
Версия: 0.3
*********************************************
********************************************/

get_header(); 

if ( have_posts() ) {
	get_template_part( 'tpl/parts/page', 'title' );
	while ( have_posts() ) {
		the_post();
		get_template_part( 'tpl/parts/page', 'content' );
	}
}else{
	get_template_part( 'tpl/parts/404', 'title' );
	get_template_part( 'tpl/parts/404', 'content' );
}
get_footer(); 
?>