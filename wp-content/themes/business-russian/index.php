<?php
/********************************************
*********************************************
Основной шаблон вывода данных пользователю
Дата: 23/08/2015
Версия: 0.2
*********************************************
********************************************/

get_header(); 

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'content', get_post_format() );
	}
}else{
	get_template_part( 'tpl/content/none' );
}
/*if( is_front_page() || is_home()) //Шаблон вывода контента главной страницы
	get_template_part( 'tpl/parts/home' ); 
/*elseif( is_tax( 'catalog' ) ) //Основной шаблон вывода списка записей таксономии "catalog"
	get_template_part( 'tpl/parts/catalog' ); 
elseif( is_post_type_archive( 'product' ) ) //Основной шаблон вывода списка архива записей произвольного типа product
	get_template_part( 'tpl/parts/catalog' ); 
*/
	
	

?> 
<?php //get_sidebar(); // Подключаем сайдбар ?>
<?php get_footer(); // Подключаем футер ?>