<?php
/************************************
Шаблон вывода контента главной страницы
Дата: 23/08/2015
Версия: 0.3
************************************/
?>
<div class="title-page">
	<div class="container">
		<div class="row">
			<?php get_template_part( 'template/tmp', 'catalog-filter' ); ?>
		</div>	
	</div>
</div>
<?php
//¬ывод слайдера 
echo do_shortcode("[metaslider id=6]"); 

$posts = get_posts( array(
	'include'		=> '70 37',// —писок id страниц которые будут выводитьс¤ на главной
	'post_type'		=> 'page',
	'orderby'         => 'post_date',
	'order'           => 'DESC',
	'post_status'	=> 'publish'
) );
//ѕровер¤ем, найдены ли данные постов, если да то выводим, иначе выводим заглушку ничего не найдено;
if (count($posts) > 0) :
	// ќбработка вывода контента главной страниц
	foreach($posts as $post){ 
		setup_postdata($post);
		// формат вывода ?>
		<h2 class="container-fluid text-center home-header">
			<?php the_title(); ?>
		</h3>
		<div class="container">
			<?php the_content(); ?>
		</div>
	<?php }
	wp_reset_postdata();
	?>
	<script>
		/* ѕри наведение на елемент
		jQuery('.home.catalog-list>li').hover(function(){
			grayscale = jQuery(this).find('.grayscale');
			if (!grayscale.hasClass('grayscale-off')) grayscale.addClass('grayscale-off');
			left = jQuery(this).parent().width()-jQuery(this).width()-10;
			if ( jQuery(this).position().left < left )
				jQuery(this).find('.sub').addClass('show-left');
			else
				jQuery(this).find('.sub').addClass('show-right');
		},function(){
			grayscale = jQuery(this).find('.grayscale');
			if (grayscale.hasClass('grayscale-off')) grayscale.removeClass('grayscale-off');
			jQuery(this).find('.sub').removeClass('show-left').removeClass('show-right');		
		});
		jQuery('.home.catalog-list .close').on('click',function(){
			jQuery(this).parents('.sub').removeClass('show-left').removeClass('show-right');		
		});
		*/
		/**** ќбрабатываем нажати¤ на пункт списка каталогов ****/		 
		jQuery('.home.catalog-list>li>a').click(function(event){
			// ќтключаю действие ссылки по умолчанию
			event.preventDefault();
			// определ¤ем повторное нажантие на блок, 
			// путем определени¤ присутстви¤ у родител¤ соответствующих классов
			// ≈сли оно есть, то удал¤ем соответствующие классы у родител¤
			// » прерываем выполнение функции
			parent = jQuery(this).parent()
			if(parent.hasClass('show-left') || parent.hasClass('show-right')) {
				parent.removeClass('show-left').removeClass('show-right');
				return false;
			}
			//в переменную заган¤ю ссылку на обернутый массив картинок категории в ч/б режиме
			grayscale = jQuery(this).siblings('.grayscale');
			//≈сли у картинок нет класса grayscale-off, то добавл¤ем его
			//Ёто позвол¤ет отключить ч/б режим дл¤ картинки
			if (!grayscale.hasClass('grayscale-off')) grayscale.addClass('grayscale-off');
						
			//ќпредел¤ем максимальный отступ дл¤ меню слева относительно родительского элемента
			left = parent.parent().width()-parent.width()-10;
			//≈сли позици¤ выбраного блока списка каталога находитс¤ до максимальный отступ дл¤ меню слева 
			if ( parent.position().left < left )
				parent.addClass('show-left'); //то присваиваем класс вывода меню слева от выбраного блока  
			else
				parent.addClass('show-right'); //иначе присваиваем класс вывода меню справа от выбраного блока		
		});
		/**** ќбрабатываем наведени¤ на пункт списка каталогов ****/
		jQuery('.home.catalog-list>li').hover(function(){
			grayscale = jQuery(this).find('.grayscale');
			if (!grayscale.hasClass('grayscale-off')) grayscale.addClass('grayscale-off');
		},function(){
			if(jQuery(this).hasClass('show-left')) return false;
			if(jQuery(this).hasClass('show-right')) return false;			
			grayscale = jQuery(this).find('.grayscale');
			if (grayscale.hasClass('grayscale-off')) grayscale.removeClass('grayscale-off');
		});
		
		/**** ќбрабатываем нажатин на кнопку закрыпи¤ поп-ап меню списка каталогов ****/
		jQuery('.home.catalog-list .close').on('click',function(){
			jQuery(this).parents('.show-left, .show-right').removeClass('show-left').removeClass('show-right');		
		});
		/**** ќбрабатываем нажатин на любую облать страницы ****/
		jQuery( "body" ).click(function( event ) {
			// ≈сли нажата люба¤ область страницы, за исключением елемента списка каталогов,
			// ¬се открытые поп-ап	меню и выбраные элементы по клику закрываютс¤.
			jQuery('.home.catalog-list .show-left,.home.catalog-list .show-right').not(jQuery(event.target).parents('.home.catalog-list>li')).removeClass('show-left').removeClass('show-right').find('.grayscale').removeClass('grayscale-off');		
			
		});
	</script>
<?php else:	
	 get_template_part( 'template/tmp', 'none' );
endif; ?>
