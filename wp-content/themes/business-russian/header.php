<?php global $theme; ?>
<!DOCTYPE html>

<html <?php language_attributes();?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width">
		<meta content="DStaroselskiy (IT-Star company)" name="author" />
		<meta content="noindex,nofollow" name="Robots"/>
		<link rel="icon" href="<?php echo get_template_directory_uri();?>/images/favicon.ico" type="image/x-icon" />
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri();?>/images/favicon.ico" type="image/x-icon" />
		 <!--[if lt IE 9]>
		 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		 <![endif]-->
		<!--title>
		<?php // Генерируем тайтл в зависимости от контента с разделителем " | "
			global $page, $paged;
			wp_title( '|', true, 'right' );
			bloginfo( 'name' );
			$site_description = get_bloginfo( 'description', 'display' );
			if ( $site_description && ( is_home() || is_front_page() ) )
				echo " | $site_description";
			if ( $paged >= 2 || $page >= 2 )
				echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
		?>
		</title-->
		<?php
			wp_head(); // Необходимо для работы плагинов и функционала wp
		?>
	</head>
	<body <?php body_class(); ?>>
		<header class="title-main">
			<div class="theme-container">
				<?php wp_nav_menu( array(
					'theme_location'  => 'primary',
					'container'       => false, 
					'menu_class'      => '', 
					'menu_id'         => '',
					'echo'            => true,
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				) ); ?>
			</div>
		</header>
		<div id="home-site-title-screen" style="background-image:url(<?php header_image();?>);">
				<?php the_custom_logo(); ?>
				<h1 style="color:<?php echo get_theme_mod( 'site-title-color','#FFFFFF' );?>"><?php echo bloginfo('name');?></h1>
				<h2 style="color:<?php echo get_theme_mod( 'site-description-color','#A7A7A7' );?>"><?php echo bloginfo('description');?></h2>
		</div>
		<div id="page-body">
