<?php
/********************************************
*********************************************
Основной шаблон вывода Главной страницы 
При настройках отображения главной страницы через админку.
В пункте "На главной странице отображать" на странице (Настройки->Чтение)
Выбрано "Ваши последние записи"
Если выбрано "Статическую страницу", то файл обработчик вывода - front_page.php
Дата: 23/08/2015
Версия: 0.1
*********************************************
********************************************/

get_header(); 

get_template_part( 'tpl/parts/home' ); 

get_footer(); 

?>
